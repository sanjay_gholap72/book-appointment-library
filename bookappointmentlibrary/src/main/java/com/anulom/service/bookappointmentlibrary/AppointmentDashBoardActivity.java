package com.anulom.service.bookappointmentlibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AppointmentDashBoardActivity extends AppCompatActivity implements View.OnClickListener
{

    Button btnBookApp,btnBookAppDetilas,btnBookAppCancel;
    ProgressDialog progressDialog;

    String docID;
    BasicDetailsReq response;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_dash_board);

        Bundle bundle = getIntent().getExtras();
        docID = bundle.getString("docID");


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);

        response = new BasicDetailsReq();


        btnBookApp = findViewById(R.id.btn_bappointment);
        btnBookAppDetilas = findViewById(R.id.btn_bappointmentdetails);


        btnBookApp.setOnClickListener(this);
        btnBookAppDetilas.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v == btnBookApp)
        {
            Intent intent = new Intent(AppointmentDashBoardActivity.this,ChooseCityActivity.class);
            intent.putExtra("docID",docID);
            startActivity(intent);
        }
        else if(v == btnBookAppDetilas)
        {
            Intent intent = new Intent(AppointmentDashBoardActivity.this, AppointmentDetailsActivity.class);
            intent.putExtra("docID",docID);
            startActivity(intent);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //change your View from A3 to A2
    }

}