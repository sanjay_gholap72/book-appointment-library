package com.anulom.service.bookappointmentlibrary;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.anulom.service.bookappointmentlibrary.BasicDetailsReq.getAppointmentDetails;
import static com.anulom.service.bookappointmentlibrary.BasicDetailsReq.get_auth_token;

public class AppointmentDetailsActivity extends AppCompatActivity
{

    RecyclerView recyclerView;
    String docID;

    BasicDetailsReq basicDetailsReq;
    AppointmentDetailsPojo appointmentDetailsPojo;
    List<AppointmentList> appointmentLists;
    List<AttendeesList> attendeesLists;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_details);

        Bundle bundle = getIntent().getExtras();
        docID = bundle.getString("docID");

        recyclerView = findViewById(R.id.recycler_appointment);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        basicDetailsReq = new BasicDetailsReq();
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);


        FeatchAppointmentDetails featchAppointmentDetails = new FeatchAppointmentDetails();
        featchAppointmentDetails.execute();
    }

    private class FeatchAppointmentDetails extends AsyncTask<String, String, String>
    {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings)
        {

            String strResponsePost = "";

            try
            {
                String url = getAppointmentDetails+"document="+docID+"&get_auth_token="+get_auth_token;
                strResponsePost = basicDetailsReq.doGetRequest(url);
            }
            catch (Exception e)
            {
                Log.wtf("e",e);
            }

            return strResponsePost;
        }

        @Override
        protected void onPostExecute(String result)
        {

            if(result==null || result.equals(""))
            {
                new SweetAlertDialog(AppointmentDetailsActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText("Something went wrong please try again ")
                        .show();
            }
            else
            {
                super.onPostExecute(result);
                try
                {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(result);
                    String jStrResult = jsonObject.getString("status");
                    JSONArray arr = jsonObject.getJSONArray("appointments");

                    if(arr.length() == 0)
                    {
                        Toast.makeText(AppointmentDetailsActivity.this,"you don't have any book appointments",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(AppointmentDetailsActivity.this,AppointmentDashBoardActivity.class);
                        intent.putExtra("docID",docID);
                        startActivity(intent);

                    }
                    else
                    {
                        if(jStrResult.equals("1"))
                        {
                            String temp = result;
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            Gson gson = gsonBuilder.create();
                            {
                                appointmentDetailsPojo = gson.fromJson(temp,AppointmentDetailsPojo.class);
                                recyclerView.setAdapter(new AppointmentAdapter(AppointmentDetailsActivity.this,appointmentDetailsPojo));
                            }
                        }
                    }


                }
                catch (Exception e)
                {
                    Log.wtf("e",e);
                }
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(AppointmentDetailsActivity.this,AppointmentDashBoardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("docID",docID);
        startActivity(intent);





    }
}