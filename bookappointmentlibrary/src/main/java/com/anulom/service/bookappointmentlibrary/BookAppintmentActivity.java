package com.anulom.service.bookappointmentlibrary;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.anulom.service.bookappointmentlibrary.BasicDetailsReq.APPOINTMENTURL;
import static com.anulom.service.bookappointmentlibrary.BasicDetailsReq.get_auth_token;

public class BookAppintmentActivity extends AppCompatActivity implements View.OnClickListener
{
    String slotID,region,subregion,textonButton,dte,docID;
    TextView txtBdte,txtBtime,txtBRegion,txt_docID,txt_more,txt_remove;

    EditText edtContactPersonName,edtContactPersonMail,edtContactPersonPhone,edtContactPersonAddress;
    EditText edtFAttendeesName,edtFAttendeesEmail,edtFAttendeesMobile;
    EditText edtSAttendeesName,edtSAttendeesEmail,edtSAttendeesMobile;
    LinearLayout linearLayout;
    EditText editTextName, editTextPhone,editTextMail;

    List<EditText> ListEditTextName;
    List<EditText> ListEditTextPhone;
    List<EditText> ListEditTextMail;


    String strContactPersonName,strContactPersonMail,strContactPersonPhone,strContactPersonAddress;
    String strFAttendeesName,strFAttendeesEmail,strFAttendeesMobile;
    String strSAttendeesName,strSAttendeesEmail,strSAttendeesMobile;

    ArrayList<String> countAttendeesName,countAttendeesEmail,countAttendeesMobile;
    String tempAttendeesName,tempAttendeesEmail,tempAttendeesMobile;


    int count = 0;
    int Fattendes = 0;
    int Sattendes = 0;
    int flag = 0;


    String firstAttendes = "F";
    String secondAttendes = "F";
    String noAttendes = "T";


    Button btnNext;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String phonePattern = "^[+]?[0-9]{10,13}$";

    ProgressDialog progressDialog;
    BasicDetailsReq response;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_appintment);

        Bundle bundle = getIntent().getExtras();
        slotID = bundle.getString("slotID");
        region = bundle.getString("region");
        subregion = bundle.getString("subregion");
        textonButton = bundle.getString("txt");
        dte = bundle.getString("dte");
        docID = bundle.getString("docID");

        txtBdte = findViewById(R.id.txt_bdte);
        txtBtime = findViewById(R.id.txt_btime);
        txtBRegion = findViewById(R.id.txt_bregion);
        txt_docID = findViewById(R.id.txt_docID);
        linearLayout = findViewById(R.id.ll_attendes);
        txt_more = findViewById(R.id.txt_more);
        txt_remove = findViewById(R.id.txt_remove);

        txtBdte.setText("Appointment Date : "+dte);
        txtBtime.setText("Time : "+textonButton);
        txtBRegion.setText("City : "+region+"\n\nSubCity : "+subregion);
        txt_docID.setText("DocID : "+docID);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);

        response = new BasicDetailsReq();

        ListEditTextName = new ArrayList<EditText>();
        ListEditTextMail = new ArrayList<EditText>();
        ListEditTextPhone = new ArrayList<EditText>();


        edtContactPersonName = findViewById(R.id.edt_contactperson);
        edtContactPersonMail = findViewById(R.id.edt_contactmail);
        edtContactPersonPhone = findViewById(R.id.edt_contactmobile);

        edtContactPersonAddress = findViewById(R.id.edt_contactaddress);

        edtFAttendeesName = findViewById(R.id.edt_fattendees_name);
        edtFAttendeesEmail = findViewById(R.id.edt_femail);
        edtFAttendeesMobile = findViewById(R.id.edt_fmobile);

        edtSAttendeesName = findViewById(R.id.edt_sattendees_name);
        edtSAttendeesEmail = findViewById(R.id.edt_semail);
        edtSAttendeesMobile = findViewById(R.id.edt_smobile);

        btnNext = findViewById(R.id.btn_next);

        countAttendeesName = new ArrayList<String>();
        countAttendeesEmail = new ArrayList<String>();
        countAttendeesMobile = new ArrayList<String>();

        txt_more.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        txt_remove.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v == btnNext)
        {
            SaveAppointmentData();
        }
        else if(v == txt_more)
        {
            AddAttendes();
        }
        else if(v == txt_remove)
        {

            int linearLayoutcount  = linearLayout.getChildCount();
            if(linearLayoutcount !=0)
            {
                RemoveAttendes();
            }
            else if(linearLayoutcount == 0)
            {
                count = 0;
            }
        }
    }

    private void RemoveAttendes()
    {
        count = count - 1;
        linearLayout.removeViewAt(linearLayout.getChildCount()-1);
        linearLayout.removeViewAt(linearLayout.getChildCount()-1);
        linearLayout.removeViewAt(linearLayout.getChildCount()-1);
        linearLayout.removeViewAt(linearLayout.getChildCount()-1);
        linearLayout.removeViewAt(linearLayout.getChildCount()-1);
    }


    @SuppressLint("ResourceAsColor")
    private void AddAttendes()
    {
        count = count + 1;


        String color = getString(Integer.parseInt(String.valueOf(R.color.colorPrimary)));


        View v = new View(this);
        v.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2));
        v.setBackgroundColor(getColor(R.color.colorPrimary));
        v.layout(0,50,0,0);


        TextView textView = new TextView(this);
        textView.setHint("\n Attendees Details: ");
        textView.layout(8,65,8,8);
        textView.setTextColor(Color.parseColor(color));


        editTextName = new EditText(this);
        editTextName.setHint("Full name");
        editTextName.setId(count);
        editTextName.layout(8,35,8,8);
        ListEditTextName.add(editTextName);

        editTextMail = new EditText(this);
        editTextMail.setHint("Email");
        editTextMail.setId(count);
        editTextMail.layout(8,15,8,8);
        ListEditTextMail.add(editTextMail);

        editTextPhone = new EditText(this);
        editTextPhone.setHint("Mobile Number");
        editTextPhone.setId(count);
        editTextPhone.layout(0,15,15,20);
        ListEditTextPhone.add(editTextPhone);

        TextView textViewspace = new TextView(this);
        textViewspace.setHint("\n");



        linearLayout.addView(textView);
        linearLayout.addView(editTextName);
        linearLayout.addView(editTextMail);
        linearLayout.addView(editTextPhone);
        linearLayout.addView(textViewspace);

    }

    private void SaveAppointmentData()
    {
        strContactPersonName = edtContactPersonName.getText().toString().trim();
        strContactPersonMail = edtContactPersonMail.getText().toString().trim();
        strContactPersonPhone = edtContactPersonPhone.getText().toString().trim();

        strContactPersonAddress = edtContactPersonAddress.getText().toString().trim();

        strFAttendeesName = edtFAttendeesName.getText().toString().trim();
        strFAttendeesEmail = edtFAttendeesEmail.getText().toString().trim();
        strFAttendeesMobile = edtFAttendeesMobile.getText().toString().trim();

        strSAttendeesName = edtSAttendeesName.getText().toString().trim();
        strSAttendeesEmail = edtSAttendeesEmail.getText().toString().trim();
        strSAttendeesMobile = edtSAttendeesMobile.getText().toString().trim();

        if(TextUtils.isEmpty(strContactPersonName))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Enter Contact Person Name")
                    .show();
        }
        else if(TextUtils.isEmpty(strContactPersonPhone))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Enter Contact Person Phone Number")
                    .show();
        }
        else if(!strContactPersonPhone.matches(phonePattern))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Invalid Phone Number")
                    .show();
        }

        else if(TextUtils.isEmpty(strContactPersonMail))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Enter contact Person Mail")
                    .show();
        }
        else if(!strContactPersonMail.matches(emailPattern))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Invalid Email Address")
                    .show();
        }
        else if(TextUtils.isEmpty(strContactPersonAddress))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Enter Biometric Address")
                    .show();
        }
        else
        {
            if(!TextUtils.isEmpty(strFAttendeesName))
            {
                if(TextUtils.isEmpty(strFAttendeesEmail))
                {
                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText("Enter attendess email")
                            .show();
                }
                else if(!strFAttendeesEmail.matches(emailPattern))
                {
                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText("Invalid email address")
                            .show();
                }
                else if(TextUtils.isEmpty(strFAttendeesMobile))
                {
                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText("Enter attendess mobile")
                            .show();
                }
                else if(!strFAttendeesMobile.matches(phonePattern))
                {
                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText("Please enter valided phone number")
                            .show();
                }
                else
                {
                    Fattendes = 1;
                    firstAttendes = "T";
                }
            }
            else
            {
                Fattendes = 1;
            }

            if(!TextUtils.isEmpty(strSAttendeesName))
            {
                if(TextUtils.isEmpty(strSAttendeesEmail))
                {
                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText("Enter attendess email")
                            .show();
                }
                else if(!strSAttendeesEmail.matches(emailPattern))
                {
                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText("Invalid email address")
                            .show();
                }
                else if(TextUtils.isEmpty(strSAttendeesMobile))
                {
                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText("Enter attendess mobile")
                            .show();
                }
                else if(!strSAttendeesMobile.matches(phonePattern))
                {
                    new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error")
                            .setContentText("Please enter valided phone number")
                            .show();
                }
                else
                {
                    Sattendes = 1;
                    secondAttendes = "T";
                }
            }
            else
            {
                Sattendes = 1;
            }

            if(count != 0)
            {
                for(int i=0;i<count;i++)
                {

                    tempAttendeesName = ListEditTextName.get(i).getText().toString();
                    tempAttendeesEmail = ListEditTextMail.get(i).getText().toString();
                    tempAttendeesMobile = ListEditTextPhone.get(i).getText().toString();


                    if(TextUtils.isEmpty(tempAttendeesName))
                    {
                        flag = 0;
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Enter attendess name")
                                .show();

                        break;
                    }
                    else if(TextUtils.isEmpty(tempAttendeesEmail))
                    {
                        flag = 0;
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Enter attendess email")
                                .show();
                        break;
                    }
                    else if(!tempAttendeesEmail.matches(emailPattern))
                    {
                        flag = 0;
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Invalid email address")
                                .show();
                        break;
                    }
                    else if(TextUtils.isEmpty(tempAttendeesMobile))
                    {
                        flag = 0;
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Enter attendess Mobile")
                                .show();
                        break;
                    }
                    else if(!tempAttendeesMobile.matches(phonePattern))
                    {
                        flag = 0;
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Please enter valided phone number")
                                .show();

                        break;
                    }
                    else
                    {
                        countAttendeesName.add (ListEditTextName.get(i).getText().toString());
                        countAttendeesMobile.add(ListEditTextPhone.get(i).getText().toString());
                        countAttendeesEmail.add(ListEditTextMail.get(i).getText().toString());
                        flag = 1;
                    }

                }
            }
            else
            {
                flag = 1;
            }


            if(Fattendes == 1)
            {
                if(Sattendes == 1)
                {
                    if(flag == 1)
                    {
                        progressDialog.show();
                        FeatchAppointmentDetails featchAppointmentDetails = new FeatchAppointmentDetails();
                        featchAppointmentDetails.execute();
                    }
                }
            }


        }
    }

    private class FeatchAppointmentDetails extends AsyncTask<String, String, String>
    {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings)
        {
            String strResponsePost = "";
            JSONArray jsonArrayPerson = new JSONArray();
            try
            {
                if(Fattendes == 1)
                {
                    if(firstAttendes.equals("T"))
                    {
                        JSONObject  jsonAttendees1 = new JSONObject();
                        jsonAttendees1.put("name",strFAttendeesName);
                        jsonAttendees1.put("email",strFAttendeesEmail);
                        jsonAttendees1.put("contact",strFAttendeesMobile);

                        jsonArrayPerson.put(jsonAttendees1);

                        noAttendes = "F";
                    }

                }

                if(Sattendes == 1)
                {
                    if(secondAttendes.equals("T"))
                    {
                        JSONObject  jsonAttendees2 = new JSONObject();
                        jsonAttendees2.put("name",strSAttendeesName);
                        jsonAttendees2.put("email",strSAttendeesEmail);
                        jsonAttendees2.put("contact",strSAttendeesMobile);

                        jsonArrayPerson.put(jsonAttendees2);

                        noAttendes ="F";
                    }

                }

                if(count!=0)
                {
                    for(int j = 0 ;j<count ;j++)
                    {
                        JSONObject  jsonAttendees = new JSONObject();
                        jsonAttendees.put("name",countAttendeesName.get(j));
                        jsonAttendees.put("email",countAttendeesEmail.get(j));
                        jsonAttendees.put("contact",countAttendeesMobile.get(j));
                        jsonArrayPerson.put(jsonAttendees);

                        noAttendes = "F";
                    }

                }

                if(noAttendes.equals("T"))
                {
                    JSONObject  jsonAttendees1 = new JSONObject();
                    jsonAttendees1.put("name",strContactPersonName);
                    jsonAttendees1.put("email",strContactPersonMail);
                    jsonAttendees1.put("contact",strContactPersonPhone);

                    jsonArrayPerson.put(jsonAttendees1);
                }

                JSONObject jsonAppointmentdtl= new JSONObject();
                jsonAppointmentdtl.put("request_no",docID);
                jsonAppointmentdtl.put("city",region);
                jsonAppointmentdtl.put("region_id",subregion);
                jsonAppointmentdtl.put("slot_id",slotID);
                jsonAppointmentdtl.put("address",strContactPersonAddress);
                jsonAppointmentdtl.put("contact_person",strContactPersonName+","+strContactPersonMail+","+strContactPersonPhone);
                jsonAppointmentdtl.put("app_flag","1");
                jsonAppointmentdtl.put("attendees",jsonArrayPerson);

                Log.wtf("jsonAppointmentdtl", String.valueOf(jsonAppointmentdtl));



                JSONObject jsonObjectToken = new JSONObject();
                jsonObjectToken.put("token",get_auth_token);

                JSONObject main = new JSONObject();
                main.put("appointment",jsonAppointmentdtl);
                main.put("auth_token",jsonObjectToken);

                String json = "";
                json = main.toString();

                strResponsePost = response.doPostRequest(APPOINTMENTURL, json);


            } catch (JSONException | IOException e)
            {
                e.printStackTrace();
                Log.wtf("e",e);
            }

            return strResponsePost;
        }

        @Override
        protected void onPostExecute(String result)
        {

            Log.wtf("result",result);

            if(result.isEmpty())
            {
                new SweetAlertDialog(BookAppintmentActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText("Something went wrong please try again ")
                        .show();
            }
            else
            {
                progressDialog.dismiss();
                try
                {
                    JSONObject jsonObject = new JSONObject(result);
                    String jStrResult = jsonObject.getString("status");

                    if(jStrResult.equals("1"))
                    {
                        Toast.makeText(BookAppintmentActivity.this, "Anulom team will confirm your appointment within 4 business hours.",Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(BookAppintmentActivity.this,AppointmentDashBoardActivity.class);
                        Intent i=new Intent(Intent.ACTION_MAIN);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("docID",docID);
                        startActivity(intent);

                    }
                    else if(jStrResult.equals("0"))
                    {
                        new SweetAlertDialog(BookAppintmentActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Please try again")
                                .show();
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }

        }
    }
}