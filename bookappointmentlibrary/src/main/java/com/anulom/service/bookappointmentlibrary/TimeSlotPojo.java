package com.anulom.service.bookappointmentlibrary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimeSlotPojo
{
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("time_slot")
    @Expose
    private List<TimeSlot> timeSlot = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<TimeSlot> getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(List<TimeSlot> timeSlot) {
        this.timeSlot = timeSlot;
    }

}
